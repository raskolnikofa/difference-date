<?php
class Difference{

	protected $years;
	protected $months;
	protected $days;

	protected $ent_months;

	public $extra_days;
	protected $total_days;
	protected $invert = FALSE;

	public function __construct($firstDate, $secondDate){
		$firstDate = explode('-', $firstDate);
		$secondDate = explode('-', $secondDate);

		$this->years = $firstDate[0] - $secondDate[0];
		$this->months = $firstDate[1] - $secondDate[1];
		$this->days = $firstDate[2] - $secondDate[2];

		$this->ent_months = array($firstDate[1], $secondDate[1]);
		$this->extra_days = $this->getExtraDays();
		$this->total_days = $this->getTotalDays();
		$this->invert = $this->getInvert();
	}

	public function getExtraDays(){

		//intercalary

		for ($i=0; $i <= $this->years ; $i++) { 
			if (is_int($i/4)) {
				$this->extra_days += 1;
			}
		}

		//month difference

		if($this->ent_months[0] > $this->ent_months[1]){

			for ($i=$this->ent_months[0]; $i <= $this->ent_months[1] ; $i++) {
				if ($i = 1 || $i = 3 || $i = 5 || $i = 7 || $i = 8 || $i = 3 || $i = 10 || $i = 12) {
					$this->extra_days += 1;
				}elseif ($i = 2) {
					$this->extra_days -= 2;
				}
			}

		}else{

			for ($i=$this->ent_months[1]; $i <= $this->ent_months[0] ; $i++) {
				if ($i = 1 || $i = 3 || $i = 5 || $i = 7 || $i = 8 || $i = 3 || $i = 10 || $i = 12) {
					$this->extra_days += 1;
				}elseif ($i = 2) {
					$this->extra_days -= 2;
				}
			}

		}

		return $this->extra_days;
	}

	public function getTotalDays(){
		return $this->total_days = 
		$this->years*365 + 
		$this->months*30 + 
		$this->days + 
		$this->extra_days;
	}

	public function getInvert(){
		if($this->total_days > 0){
			return $this->invert = TRUE;
		}
	}

	public function getDifference(){
		echo 'Difference between dates <br>'. 
		'Years: '. $this->years. '<br>' .
		'Months: '. $this->months. '<br>' .
		'Days: '. $this->days. '<br>' .
		'Total days: '. $this->total_days. '<br>' .
		'Invert: '. $this->invert. '<br>' ;
		
	}
}