<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once ('Difference.class.php');

$firstDate = '1955-03-07';
$secondDate = '1945-10-07';

$dates = new Difference($firstDate, $secondDate);
$dates->getDifference();
